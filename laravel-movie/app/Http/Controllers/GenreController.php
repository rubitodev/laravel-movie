<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Genres;

class GenreController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $genres = Genres::latest()->get();
 
        //test
        //dd($Genress);

        //make response JSON
        return response()->json([
            'success' => true,
            'message' => 'List Data Genres',
            'data' => $genres
        ], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validator = Validator::make($request->all(), [
            'nama' => 'required'

        ]);

        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

         //save to database
        $genres = Genres::create([
            'nama' => $request->nama
        ]);
 
         //success save to database
        if ($genres) {

            return response()->json([
                'success' => true,
                'message' => 'Genres Created',
                'data' => $genres
            ], 201);

        }

        //failed save to database
        return response()->json([
            'success' => false,
            'message' => 'Genres Failed to Save',
        ], 409);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $genres = Genres::find($id);

        if ($genres) {   
            //make response JSON
            return response()->json([
                'success' => true,
                'message' => 'Detail Data Genres',
                'data' => $genres
            ], 200);
        }


        return response()->json([
            'success' => false,
            'message' => 'Data dengan id: ' . $id . ' tidak ditemukan',
        ], 404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $allRequest = $request->all();

        $validator = Validator::make($allRequest, [
            'nama' => 'required',
        ]);
         
         //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        //find Genres by ID
        $genres = Genres::find($id);

        if ($genres) {
 
             //update Genres
            $genres->update([
                'nama' => $request->nama
            ]);

            return response()->json([
                'success' => true,
                'message' => 'Genres with title ' . $genres->nama . ' Updated',
                'data' => $genres
            ], 200);

        }
 
         //data Genres not found
        return response()->json([
            'success' => false,
            'message' => 'Data with ' . $id . '  Not Found',
        ], 404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $genres = Genres::find($id);

        if ($genres) {
 
             //delete Genres
            $genres->delete();

            return response()->json([
                'success' => true,
                'message' => 'Genres Deleted',
            ], 200);

        }
 
         //data Genres not found
        return response()->json([
            'success' => false,
            'message' => 'Genres Not Found',
        ], 404);
    }
}
