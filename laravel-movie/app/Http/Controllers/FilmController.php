<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Films;
use App\Genres;

class FilmController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $films = Films::latest()->get();
 
        //make response JSON
        return response()->json([
            'success' => true,
            'message' => 'List Data Films',
            'data' => $films
        ], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validator = Validator::make($request->all(), [
            'judul' => 'required',
            'ringkasan' => 'required',
            'tahun' => 'required',
            'poster' => 'required',
            'genre_id' => 'required',

        ]);

        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $films = Films::create([
            'judul' => $request->judul,
            'ringkasan' => $request->ringkasan,
            'tahun' => $request->tahun,
            'poster' => $request->poster,
            'genre_id' => $request->genre_id,
        ]);
 
         //success save to database
        if ($films) {

            return response()->json([
                'success' => true,
                'message' => 'Films Created',
                'data' => $films
            ], 201);

        }

        //failed save to database
        return response()->json([
            'success' => false,
            'message' => 'Films Failed to Save',
        ], 409);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $films = Films::find($id);

        if ($films) {   
            //make response JSON
            return response()->json([
                'success' => true,
                'message' => 'Detail Data Films',
                'data' => $films
            ], 200);
        }


        return response()->json([
            'success' => false,
            'message' => 'Data dengan id: ' . $id . ' tidak ditemukan',
        ], 404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $allRequest = $request->all();

        $validator = Validator::make($allRequest, [
            'judul' => 'required',
            'ringkasan' => 'required',
            'tahun' => 'required',
            'poster' => 'required',
            'genre_id' => 'required',
        ]);
         
         //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        //find Films by ID
        $films = Films::find($id);

        if ($films) {
 
             //update Films
            $films->update([
                'judul' => $request->judul,
                'ringkasan' => $request->ringkasan,
                'tahun' => $request->tahun,
                'poster' => $request->poster,
                'genre_id' => $request->genre_id,
            ]);

            return response()->json([
                'success' => true,
                'message' => 'Films with title ' . $films->judul . ' Updated',
                'data' => $films
            ], 200);

        }
 
         //data Films not found
        return response()->json([
            'success' => false,
            'message' => 'Data with ' . $id . '  Not Found',
        ], 404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $films = Films::find($id);

        if ($films) {
 
             //delete Films
            $films->delete();

            return response()->json([
                'success' => true,
                'message' => 'Films Deleted',
            ], 200);

        }
 
         //data Films not found
        return response()->json([
            'success' => false,
            'message' => 'Films Not Found',
        ], 404);
    }
}
